from rest_framework import serializers
from piramide.models import Person,Payment


class PersonSerializers(serializers.ModelSerializer):
    total_paid = serializers.IntegerField(read_only=True)
    total_earned = serializers.IntegerField(read_only=True)
    total_count = serializers.IntegerField(read_only=True)
    user_profile = serializers.PrimaryKeyRelatedField(read_only=True)
    class Meta:
        model = Person
        fields = ('name','total_count','parent','id','total_paid','total_earned','user_profile',)

    def create(self, validated_data):
        person = Person.objects.create(**validated_data)
        person.user_profile = self.context['request_user']
        person.save()
        return person


class PaymentSerializers(serializers.ModelSerializer):
    person = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Payment
        fields = ('id', 'count', 'person')

    def create(self, validated_data):
        payment = Payment.objects.create(**validated_data)    
        person = Person.objects.get(user_profile=self.context['request'])
        payment.person = person
        payment.save()
        return payment


# class PaymentSerializers(serializers.ModelSerializer):
#     person = serializers.PrimaryKeyRelatedField(read_only=True)

#     class Meta:
#         model = Payment
#         fields = ('count','person','id')

#     def create(self, validated_data):
#         payment = Payment.objects.create(**validated_data)
#         person = Person.objects.get(user_profile=self.context['request_user'])
#         payment.person = person
#         payment.save()
#         return payment


class PersonDetailSerializers(serializers.ModelSerializer):
    person_payment = serializers.IntegerField(read_only=True)
    total_count = serializers.IntegerField(read_only=True)
    parent = serializers.StringRelatedField(read_only=True)
    total_paid = serializers.IntegerField(read_only=True)
    total_earned = serializers.IntegerField(read_only=True)

    class Meta:
        model = Person
        fields = ('name','total_count','parent','id','person_payment','total_paid','total_earned')