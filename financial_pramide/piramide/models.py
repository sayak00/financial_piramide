from django.db import models
from django.conf import settings


class Person(models.Model):
    user_profile = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, blank=True, null=True)
    name = models.CharField(verbose_name='Имя',max_length=32)
    total_count = models.PositiveIntegerField(verbose_name='Счет',default=0)
    parent = models.ForeignKey('self',on_delete=models.CASCADE,verbose_name='Родитель сотрудника',related_name="parent_person",blank=True,null=True)
    total_paid = models.PositiveIntegerField(verbose_name='Общая Оплата',default=0)
    total_earned = models.PositiveIntegerField(verbose_name='Заработок от приглашенных',default=0)
    def __str__(self):
        return self.name

class Payment(models.Model):
    count = models.PositiveIntegerField(verbose_name='Внесение Суммы',default=0)
    person = models.ForeignKey(Person,on_delete=models.CASCADE,blank=True,null=True,verbose_name='Оплата Сотрудника',related_name="person_payment")