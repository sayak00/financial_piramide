from django.urls import path
from piramide.views import PersonView,PaymentView,PersonDetailView


urlpatterns =[
    path('persons/',PersonView.as_view({'get':'list','post':'create'})),
    path('persons/<int:pk>/',PersonDetailView.as_view({'get':'retrieve','put':'update','delete':'destroy'})),
    path('persons/payment',PaymentView.as_view({'get':'list','post':'create'})),
    path('persons/payment/<int:pk>/',PaymentView.as_view({'get':'retrieve','put':'update','delete':'destroy'})),
    path('persons/my/', PersonView.as_view({'get': 'my_person'})),
    path('persons/my/payments/', PaymentView.as_view({'get': 'my_payments'})),    
]
