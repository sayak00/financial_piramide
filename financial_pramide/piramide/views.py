from django.shortcuts import render
import django_filters.rest_framework
from rest_framework.viewsets import ModelViewSet
from rest_framework import filters,status
from piramide.models import Person,Payment
from piramide.serializers import PersonSerializers,PaymentSerializers,PersonDetailSerializers
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication
from .permissions import IsPersonOwnerOrGet,IsPaymentOwnerOrGet

class PersonView(ModelViewSet):
    authentication_classes = [TokenAuthentication,]
    permission_classes= [IsPersonOwnerOrGet,]
    serializer_class = PersonSerializers
    queryset = Person.objects.all()
    lookup_field = 'pk'
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend,filters.SearchFilter]
    search_fields = ('name',)

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request_user'] = self.request.user
        return context

    def my_person(self,request,*args,**kwargs):
        instance = Person.objects.get(user_profile=request.user)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


class PaymentView(ModelViewSet):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsPaymentOwnerOrGet] 
    serializer_class = PaymentSerializers
    queryset = Payment.objects.all()
    lookup_field = 'pk'


    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request'] = self.request.user
        return context

    def my_payments(self, request, *args, **kwargs):
        person = Person.objects.get(user_profile=request.user)
        instance = Payment.objects.filter(person=person)
        serializer = self.get_serializer(instance, many=True)
        return Response(serializer.data)

    def create(self,request,*args,**kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        person = Person.objects.get(user_profile=request.user)
        payment = serializer.validated_data['count']
        serializer.save()
        if person.parent:
            person.total_count += float(request.data['count'])*0.9
            person.parent.total_count += float(request.data['count'])*0.1
            person.total_paid += int(request.data['count'])
            person.parent.total_earned += float(request.data['count'])*0.1
            person.save()
            person.parent.save()
        else:
            person.total_paid += int(request.data['count'])
            person.total_count += float(request.data['count'])
            person.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


    def perform_destroy(self, instance):
        person = instance.person
        if person.parent:
            person.total_count -= float(instance.count)*0.9
            person.parent.total_count -= float(instance.count)*0.1
            person.parent.total_earned -= float(instance.count)*0.1
            person.total_paid  -= float(instance.count)
            person.parent.save()
            person.save()
        else:
            person.total_count -= float(instance.count)
            person.total_paid  -= float(instance.count)
        person.save()
        instance.delete()


    def update(self, request, *args, **kwargs):
        payment = self.get_object()
        serializer = self.get_serializer(instance=payment, data=request.data)
        serializer.is_valid(raise_exception=True)
        person = Person.objects.get(user_profile=request.user)
        if person.parent:
            person.total_count -= float(payment.count) * 0.9
            person.parent.total_count -= float(payment.count)*0.1
            person.total_count += float(request.data['count']) * 0.9
            person.parent.total_count += float(request.data['count']) * 0.1

            person.total_paid -= float(payment.count)
            person.parent.total_earned -= float(payment.count)*0.1
            person.total_paid += float(request.data['count']) 
            person.parent.total_earned += float(request.data['count'])*0.1
            person.save()
            person.parent.save()
        else: 
            person.total_count -= float(payment.count)
            person.total_count += float(request.data['count'])
            person.total_paid -= float(payment.count)
            person.total_paid += float(request.data['count'])
        person.save()
        serializer.save()  
        return Response(serializer.data,status=status.HTTP_200_OK)



class PersonDetailView(ModelViewSet):
    authentication_classes = [TokenAuthentication,]
    permission_classes= [IsPersonOwnerOrGet,IsPaymentOwnerOrGet]
    serializer_class = PersonDetailSerializers
    queryset = Person.objects.all()
    lookup_field = 'pk'


    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request'] = self.request
        return context

    def my_person(self,request,*args,**kwargs):
        instance = Person.objects.get(user_profile=request.user)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    # def perform_destroy(self, instance):
    #     person = instance.person
    #     if person.parent:
    #         person.total_count -= float(instance.count)*0.9
    #         person.parent.total_count -= float(instance.count)*0.1
    #         person.total_paid -= int(instance.count)
    #         person.parent.total_earned -= float(instance.count)*0.1
    #         person.save()
    #         person.parent.save()
    #     else:
    #         person.total_paid -= int(instance.count)
    #         person.total_count -= float(instance.count)
    #         person.save()
    #     instance.delete()


    # def update(self,request,*args,**kwargs):
    #     serializer = self.get_serializer(data=request.data)
    #     serializer.is_valid(raise_exception=True)
    #     person = Person.objects.get(user_profile=request.user)
    #     payment = self.get.object
    #     serializer.save()
    #     if person.parent:
    #         person.total_count -= float(payment.count)*0.9
    #         person.parent.total_count -= float(payment.count)*0.1
    #         person.total_count += float(request.data['count'])*0.9
    #         person.parent.total_count += float(request.data['count'])*0.1
    #         person.total_paid -= int(payment.count)
    #         person.total_paid += int(request.data['count'])
    #         person.parent.total_earned -= float(payment.count)*0.1
    #         person.parent.total_earned += float(request.data['count'])*0.1
    #         person.save()
    #         person.parent.save()
    #     else:
    #         person.total_paid -= int(payment.count)
    #         person.total_paid += int(request.data['count'])
    #         person.total_count -= float(payment.count)
    #         person.total_count += float(request.data['count'])
    #         person.save()
    #     serializer.save()
    #     return Response(serializer.data, status=status.HTTP_200_OK)

    # def get_serializer_context(self):
    #     context = super().get_serializer_context()
    #     context['request_user'] = self.request.user
    #     return context


    # def my_payments(self, request, *args, **kwargs):
    #     person = Person.objects.get(user_profile=request.user)
    #     instance = Payment.objects.filter(person=person)
    #     serializer = self.get_serializer(instance, many=True)
    #     return Response(serializer.data)

        

# class PersonDetailView(ModelViewSet):
#     authentication_classes = [TokenAuthentication,]
#     permission_classes= [IsPersonOwnerOrGet,IsPaymentOwnerOrGet]
#     serializer_class = PersonDetailSerializers
#     queryset = Person.objects.all()
#     lookup_field = 'pk'


#     def get_serializer_context(self):
#         context = super().get_serializer_context()
#         context['request'] = self.request
#         return context

#     def my_person(self,request,*args,**kwargs):
#         instance = Person.objects.get(user_profile=request.user)
#         serializer = self.get_serializer(instance)
#         return Response(serializer.data)

