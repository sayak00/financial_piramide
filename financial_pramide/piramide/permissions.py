from rest_framework.permissions import BasePermission
from .models import Person,Payment

class IsPersonOwnerOrGet(BasePermission):
    def has_permission(self, request, view):
        if request.method == 'GET' or request.method == 'POST':
            return True
        else:
            person = Person.objects.get(id=view.kwargs['pk'])
            return request.user == person.user_profile


class IsPaymentOwnerOrGet(BasePermission):
    def has_permission(self, request, view):
        if request.method == 'GET' or request.method == 'POST':
            return True
        else: 
            payment = Payment.objects.get(id=view.kwargs['pk'])
            person = payment.person
            return request.user == payment.person.user_profile