from django.contrib import admin
from piramide.models import Person,Payment

admin.site.register(Person)
admin.site.register(Payment)
